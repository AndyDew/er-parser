package main

import (
	"encoding/json"
	"fmt"
	"github.com/gocolly/colly"
	"github.com/thedevsaddam/gojsonq"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"strings"
)

type Import struct {
	Username      string `json:"username"`
	Password      string `json:"password"`
	UserAgent     string `json:"user_agent"`
	PhoneID       string `json:"phone_id"`
	Adid          string `json:"adid"`
	GUID          string `json:"guid"`
	DeviceID      string `json:"device_id"`
	XIGAppID      string `json:"X-IG-App-ID"`
	XFBHTTPEngine string `json:"X-FB-HTTP-Engine"`
	XCsrftoken    string `json:"x-csrftoken"`
}

func InArray(needle interface{}, haystack interface{}) (exists bool) {
	exists = false

	switch reflect.TypeOf(haystack).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(haystack)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(needle, s.Index(i).Interface()) == true {
				exists = true
				return
			}
		}
	}
	return
}

func main() {
	var accountID, rankToken string
	var settings Import

	target := os.Args[1]
	posts, _ := strconv.Atoi(os.Args[2])

	importJson, err := os.Open("settings.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}

	defer importJson.Close()
	importJsonBytes, _ := ioutil.ReadAll(importJson)
	json.Unmarshal(importJsonBytes, &settings)

	os.Remove("small.txt")
	os.Create("small.txt")
	small, _ := os.OpenFile("small.txt", os.O_APPEND|os.O_WRONLY, 0644)

	os.Remove("big.txt")
	os.Create("big.txt")
	big, _ := os.OpenFile("big.txt", os.O_APPEND|os.O_WRONLY, 0644)

	c := colly.NewCollector()

	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("User-Agent", settings.UserAgent)
		r.Headers.Set("X-IG-App-ID", settings.XIGAppID)
		r.Headers.Set("X-IG-Capabilities", "WIFI")
		r.Headers.Set("X-IG-Connection-Speed", "4054kbps")
		r.Headers.Set("Connection", "Keep-Alive")
		r.Headers.Set("X-FB-HTTP-Engine", settings.XFBHTTPEngine)
		r.Headers.Set("accept", "*/*")
		r.Headers.Set("accept-encoding", "gzip, deflate, br")
		r.Headers.Set("accept-language", "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7")
		r.Headers.Set("x-csrftoken", settings.XCsrftoken)
		r.Headers.Set("content-type", "application/x-www-form-urlencoded")
	})

	post := c.Post("https://i.instagram.com/api/v1/accounts/login/", map[string]string{"phone_id": settings.PhoneID, "username": settings.Username, "adid": settings.Adid, "guid": settings.GUID, "device_id": settings.DeviceID, "password": settings.Password, "login_attempt_count": "0"})
	if post != nil {
		log.Fatal(err)
		os.Exit(0)
	}

	c.OnResponse(func(r *colly.Response) {
		if r.StatusCode != 200 {
			fmt.Printf("Bad Response! %s", r.Request.Body)
		} else {
			if r.StatusCode == 429 {
				println("Throttling")
			}

			switch {
			case strings.Contains(r.Request.URL.Path, "/api/v1/users/search/"):
				accountID = fmt.Sprintf("%.0f", gojsonq.New().JSONString(string(r.Body)).From("users.[0].pk").Get())
				rankToken = gojsonq.New().JSONString(string(r.Body)).From("rank_token").Get().(string)
				fmt.Printf("Found id %s for target %s!\n", string(accountID), target)
				fmt.Printf("Parsing last %d posts..\n", posts)
				c.Visit("https://i.instagram.com/api/v1/feed/user/" + accountID)
				c.Visit("https://i.instagram.com/api/v1/friendships/" + accountID + "/followers/?rankToken=" + rankToken)
				break
			case strings.Contains(r.Request.URL.Path, "/api/v1/feed/user/"):
				items := gojsonq.New().JSONString(string(r.Body)).From("items").Select("id").Get().([]interface{})
				for i := 0; i < posts; i++ {
					id := items[i].(map[string]interface{})["id"].(string)
					fmt.Printf("Found post with id: %s\n", id)
					c.Visit("https://i.instagram.com/api/v1/media/" + id + "/likers/")
					c.Visit("https://i.instagram.com/api/v1/media/" + id + "/comments/")
				}
				break
			case strings.Contains(r.Request.URL.Path, "/likers/"):
				users := gojsonq.New().JSONString(string(r.Body)).From("users").Select("username").Get().([]interface{})
				for _, user := range users {
					small.WriteString(user.(map[string]interface{})["username"].(string) + "\n")
				}
				break
			case strings.Contains(r.Request.URL.Path, "/comments/"):
				comments := gojsonq.New().JSONString(string(r.Body)).From("comments").Select("user.username").Get().([]interface{})
				for _, comment := range comments {
					small.WriteString(comment.(map[string]interface{})["username"].(string) + "\n")
				}
				nextPage := gojsonq.New().JSONString(string(r.Body)).From("next_max_id").Get()
				if nextPage != nil {
					id := strings.Split(r.Request.URL.String(), "/")[6]
					c.Visit("https://i.instagram.com/api/v1/media/" + id + "/comments/?max_id=" + url.QueryEscape(nextPage.(string)))
				}
				break
			case strings.Contains(r.Request.URL.Path, "/followers/"):
				followers := gojsonq.New().JSONString(string(r.Body)).From("users").Select("username").Get().([]interface{})
				for _, follower := range followers {
					big.WriteString(follower.(map[string]interface{})["username"].(string) + "\n")
				}
				nextPage := gojsonq.New().JSONString(string(r.Body)).From("next_max_id").Get()
				if nextPage != nil {
					c.Visit("https://i.instagram.com/api/v1/friendships/" + accountID + "/followers/?rankToken=" + rankToken + "&max_id=" + url.QueryEscape(nextPage.(string)))
				} else {
					fmt.Printf("Followers parsing finished...\n")
				}
				break
			default:
				fmt.Printf("Unknown Endpoint\n")
				fmt.Printf("%s", r.Body)
				break
			}
		}
	})
	c.Visit("https://i.instagram.com/api/v1/users/search/?q=" + target)

	small.Close()
	big.Close()

	smallBytes, _ := ioutil.ReadFile("small.txt")
	bigBytes, _ := ioutil.ReadFile("big.txt")

	smallArrays := strings.Split(string(smallBytes), "\n")
	bigArrays := strings.Split(string(bigBytes), "\n")
	os.Remove("results.txt")
	os.Create("results.txt")
	results, _ := os.OpenFile("results.txt", os.O_APPEND|os.O_WRONLY, 0644)

	for _, bigEntry := range bigArrays {
		if !InArray(bigEntry, smallArrays) {
			results.WriteString(bigEntry + "\n")
		}
	}

	results.Close()
	fmt.Printf("Parsing finished!")
}
